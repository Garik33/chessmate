'use strict';

const NODE_ENV = process.env.NODE_ENV || 'dev';
const webpack = require('webpack');
const path    = require('path');

const plugins = [];

function getAlias(json) {

  const result = {};
  for(const name in json)
    result[name] = path.resolve(__dirname, json[name]);

  return result;
}

plugins.push(new webpack.DefinePlugin({ NODE_ENV: JSON.stringify(NODE_ENV) }));

// if (NODE_ENV === 'production')
//     minimizer.push(new UglifyJsPlugin());

module.exports = {

  mode: 'production',
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'build.js'
  },

  devtool: NODE_ENV == 'dev' ? "cheep-inline-module-source-map" : false,

  watch: NODE_ENV === 'dev',
  watchOptions: {
    aggregateTimeout: 100,
    ignored: /node_modules/
  },

  plugins: plugins,

  resolve: {
    alias: getAlias({
      '~Config' : './config',
      '~Resources' : './resources',
      '~WebCore' : './../web-core'
    })
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
};

