import * as core from '../../../work/web-core';

global.core = core;
global.helper = core.helper;

const gameConf = require('../config/gameConf');


import State from "./States";
import LauncherState from './launcher/LauncherState';
import GameState from "./game/GameState";

const app = new PIXI.Application({ width: gameConf.screen_width, height: gameConf.screen_height });

document.body.appendChild(app.view);

core.StateManager.instance().addStateType(State.LAUNCHER, LauncherState);
core.StateManager.instance().addStateType(State.GAME, GameState);

core.StateManager.instance().setScene(app.stage);

app.ticker.add(() => core.StateManager.instance().update());

function startGame() {
    core.StateManager.instance().setState(State.LAUNCHER, []);
}

function loadResources() {
    core.ResourceManager.instance().addResource('Chess');
    core.ResourceManager.instance().loadXml('Chess', 'resources/xmls/chess.xml', startGame, loader => console.log(loader.progress));
}

loadResources();