import States from "../States";
import Figure from "./Figure";
import Field from "./Field";
import Figures from "./ChessEnum";

const gameConf = require('./../../config/gameConf');

export default class GameView extends PIXI.Container {
    constructor() {
        super('Game View');

        this.res = core.ResourceManager.instance().findResource('Chess');

        const background = helper.createSprite(this.res, 'anime_wallpaper3', this, {x: 0, y: 0});

        background.width = gameConf.screen_width;
        background.height = gameConf.screen_height;
        background.interactive = true;
        background.on('mouseup', () => {
            this.ghost.visible = false;
        });

        this.container = new PIXI.Container();
        this.container.attachTo(this);
        this.container.interactive = true;
        this.container.on('mousemove', (event) => {
            this.ghost.position.x = event.data.global.x;
            this.ghost.position.y = event.data.global.y;
        });

        const dancingGirlTexture = helper.createTexture(this.res, 'anim1');
        this.dancingGirl = new PIXI.Sprite(dancingGirlTexture);
        this.dancingGirl.addTween(new PIXI.TweenAnim(dancingGirlTexture, { duration: 800, repeat: -1 }));
        this.dancingGirl.position.set(gameConf.screen_width - 260, gameConf.screen_height - 540);
        this.dancingGirl.attachTo(this);

        const sittingGirlTexture = helper.createTexture(this.res, 'sittingGirl');
        this.sittingGirl = new PIXI.Sprite(sittingGirlTexture);
        this.sittingGirl.addTween(new PIXI.TweenAnim(sittingGirlTexture, { duration: 800, repeat: -1 }));
        this.sittingGirl.position.set(gameConf.screen_width - 960, gameConf.screen_height - 1085);
        this.sittingGirl.scale.set(0.8);
        this.sittingGirl.attachTo(this);

        const ushkiGirlTexture = helper.createTexture(this.res, 'ushki');
        this.ushkiGirl = new PIXI.Sprite(ushkiGirlTexture);
        this.ushkiGirl.addTween(new PIXI.TweenAnim(ushkiGirlTexture, { duration: 800, repeat: -1 }));
        this.ushkiGirl.position.set(gameConf.screen_width - 1500, gameConf.screen_height - 100);
        this.ushkiGirl.scale.set(0.3);
        this.ushkiGirl.attachTo(this);

        this.whiteFiguresCounterObj = [];
        this.blackFiguresCounterObj = [];
        this.whiteFiguresCounterPics = [];
        this.blackFiguresCounterPics = [];

        this.ghost = helper.createSprite(this.res, Figures.WHITE, this, { x: 0, y: 0 }, { x: 0.5, y: 0.5 });
        this.ghost.width = 100;
        this.ghost.height = 100;
        this.ghost.visible = false;

        for (let i = 1; i <= 12; i++) {

        }

        this.squares = [];

        for (let i = 0; i < 8; i++) {
            this.squares[i] = [];

            for (let j = 0; j < 8; j++) {
                let fillColor = null;

                if (i % 2 === j % 2) fillColor = 0xFFE4B5;
                else fillColor = 0xA0522D;

                this.squares[i][j] = new Field(this.container, i, j, fillColor);

                this.squares[i][j].square.interactive = true;
                this.squares[i][j].square
                    .on('mousedown', () => {
                        if (this.figureAlreadySelected) {
                            this.selectPlaces(this.selectedCoords[0][0], this.selectedCoords[0][1], false);
                            this.squares[i][j].circle.visible = false;
                            this.squares[i][j].substateForMove.visible = false;
                        }
                    })
                    .on('mouseup', () => {
                        if (this.figureAlreadySelected) {
                            this.selectPlaces(this.selectedCoords[0][0], this.selectedCoords[0][1], false);
                            this.squares[i][j].circle.visible = false;
                            this.squares[i][j].substateForMove.visible = false;
                        }

                        this.ghost.visible = false;
                    })
                    .on('mouseover', () => {
                        if (this.squares[i][j].circle.visible) this.squares[i][j].toggleSubstate();
                    })
                    .on('mouseout', () => {
                       if (this.squares[i][j].substateForMove.visible) this.squares[i][j].toggleSubstate();
                    });
                this.container.addChild(this.squares[i][j]);
            }
        }

        this.container.x = gameConf.screen_width / 2 - 300;
        this.container.y = gameConf.screen_height / 2;
        this.container.pivot.x = this.container.getWidth() / 2;
        this.container.pivot.y = this.container.getHeight() / 2;

        this.textStyle = new PIXI.TextStyle({
            fontFamily: 'Arial',
            fontSize: 36,
            fontStyle: 'italic',
            fontWeight: 'bold',
            fill: ['#ffffff', '#00ff99'], // gradient
            stroke: '#4a1850',
            strokeThickness: 5,
            dropShadow: true,
            dropShadowColor: '#000000',
            dropShadowBlur: 4,
            dropShadowAngle: Math.PI / 6,
            dropShadowDistance: 6,
            wordWrap: false,
            wordWrapWidth: 440,
        });

        const exitButtonText = 'ВЫХОД';

        this.exitButton = new PIXI.Text(exitButtonText, this.textStyle);
        this.exitButton.anchor.set(0.5);
        this.exitButton.x = 1820;
        this.exitButton.y = 1040;
        this.exitButton.interactive = true;
        this.exitButton.buttonMode = true;
        this.exitButton
            .on('mousedown', () => this.buttonDown())
            .on('mouseupoutside', () => this.buttonUp())
            .on('mouseup', () => this.endGame());
        this.exitButton.attachTo(this);

        this.figureAlreadySelected = false;

        this.selectedCoords = [];

        // пример-----------------------------------------------------------------------------------
        // const figure = helper.createSprite(this.res, 'blackFigures', this, { x: 100, y: 100 }); |
        // figure.setColumn(Figures.PAWN)                                                          |
        // так можно                                                                               |
        // figure.setResAnim(helper.createTexture(this.res, 'whiteFigures'));                      |
        //------------------------------------------------------------------------------------------

        this.figures = [];

        this.whiteScore = new PIXI.Text('', this.textStyle);
        this.whiteScore.anchor.set(0.5);
        this.whiteScore.x = 115;
        this.whiteScore.y = gameConf.screen_height / 2 + 350;
        this.whiteScore.attachTo(this);

        this.blackScore = new PIXI.Text('', this.textStyle);
        this.blackScore.anchor.set(0.5);
        this.blackScore.x = 115;
        this.blackScore.y = gameConf.screen_height / 2 - 350;
        this.blackScore.attachTo(this);

        this.updateScore(0, 0);
    }

    endGame() {
        this.exitButton.scale.set(1.0);

        core.StateManager.instance().setState(States.LAUNCHER, []);
    }

    buttonDown() {
        this.exitButton.scale.set(0.9);
    }

    buttonUp() {
        this.exitButton.scale.set(1.0);
    }

    prepareBattleField(startPositions) {
        for (let i = 0; i < 8; i++) {
            this.figures[i] = [];

            for(let j = 0; j < 8; j++) {
                if (startPositions[i][j] == 0) continue;

                let figureColor = (startPositions[i][j] > 6) ? Figures.BLACK : Figures.WHITE;

                this.figures[i][j] = new Figure(startPositions, this.container, figureColor, i, j);

                // this.figures[i][j].chessFigure.on('showghost', e => this.showGhost(e.event, e.col, e.color));
                this.figures[i][j].chessFigure.on('mouseup', () => this.ghost.visible = false);
            }
        }
    }

    showGhost(event, col, color) {
        console.log('kek');
        // let col = (startPositions[i][j] > 6) ? startPositions[i][j] - 7 : startPositions[i][j] - 1;
        this.ghost.setResAnim(helper.createTexture(this.res, color));
        this.ghost.setColumn(col);
        this.ghost.position.x = event.data.global.x;
        this.ghost.position.y = event.data.global.y;
        this.ghost.visible = true;
    }

    selectPlaces(i, j, isYorFigure) {
        if (!isYorFigure && this.figureAlreadySelected) this.hideAvailiableMoves();
        if (!isYorFigure && !this.figureAlreadySelected) return;

        let i_prev = null;
        let j_prev = null;
        let i_cur = i;
        let j_cur = j;

        if (!this.figureAlreadySelected) {
            this.selectedCoords.push([i, j]);
        } else {
            i_prev = this.selectedCoords[0][0];
            j_prev = this.selectedCoords[0][1];
            this.selectedCoords.pop();
        }

        if (i_prev != null && j_prev != null) {
            i = i_prev;
            j = j_prev;
        }

        this.figureAlreadySelected = !this.figureAlreadySelected;

        if (this.figures[i][j]) this.figures[i][j].toggleSelectionFigure();

        if (isYorFigure && i_prev != null && j_prev != null) {
            if (i == i_cur && j == j_cur) {
                this.hideAvailiableMoves();
                return -1;
            } else {
                this.figures[i_cur][j_cur].toggleSelectionFigure();
                this.selectedCoords.push([i_cur, j_cur]);
                this.figureAlreadySelected = !this.figureAlreadySelected;
            }
        }
    }

    showAvailiableMoves(availiableMoves) {
        this.hideAvailiableMoves();
        availiableMoves.forEach(item => {
            this.squares[item[0]][item[1]].toggleCircles();
        });
        this.markFood();
    }

    hideAvailiableMoves() {
        this.unmarkFood();
        this.squares.forEach(row => {
            row.forEach(item => {
                if (item.circle.visible) item.toggleCircles();
            });
        });
    }

    move(figureRow, figureCol, targetRow, targetCol) {
        // console.log('view: from (' + figureRow + ', ' + figureCol + ') to (' + targetRow + ', ' + targetCol + ')');
        let temp = this.figures[figureRow][figureCol];
        this.figures[figureRow][figureCol] = this.figures[targetRow][targetCol];
        this.figures[targetRow][targetCol] = temp;
        this.figures[targetRow][targetCol].container.addTween(new PIXI.Tween({x: targetCol * 100, y: targetRow * 100}, {duration: 1000, easing: PIXI.TweenEasing.outBack}));
        this.figures[targetRow][targetCol].toggleSelectionFigure();
    }

    eat(figureRow, figureCol, targetRow, targetCol) {
        this.container.removeChild(this.figures[targetRow][targetCol].container);
        this.figures[targetRow][targetCol] = undefined;
        this.move(figureRow, figureCol, targetRow, targetCol);
    }

    markFood() {
        this.squares.forEach((row, i) => {
            row.forEach((item, j) => {
                if (item.circle.visible && this.figures[i][j]) {
                    item.toggleSubstate();
                    this.figures[i][j].showFood();
                }
            });
        });
    }

    unmarkFood() {
        this.squares.forEach((row, i) => {
            row.forEach((item, j) => {
                if (item.substateForMove.visible && this.figures[i][j]) {
                    item.toggleSubstate();
                    this.figures[i][j].food.visible = false;
                }
            });
        });
    }

    mate(winColor) {
        let winContainer = new PIXI.Container();
        winContainer.attachTo(this);

        let winBackground = new PIXI.Graphics();
        winBackground.beginFill(0xFFFFFF, 0.5);
        winBackground.drawRect(0, 0, gameConf.screen_width, gameConf.screen_height);
        winBackground.endFill();
        winBackground.interactive = true;
        winBackground.attachTo(winContainer);

        let winText = winColor + ' ПОБЕДИЛИ!';
        let winMsg = new PIXI.Text(winText, this.textStyle);
        winMsg.anchor.set(0.5);
        winMsg.x = gameConf.screen_width / 2 - 300;
        winMsg.y = gameConf.screen_height / 2;
        winMsg.attachTo(winContainer);

        winMsg.addTween(new PIXI.Tween({x: gameConf.screen_width / 2, scale: {x: 4, y: 4}}, {duration: 1000}));

        let btnDown1 = () => {
            retryButton.scale.set(0.9);
        }

        let btnDown2 = () => {
            backToMenuButton.scale.set(0.9);
        }

        let btnUp1 = () => {
            retryButton.scale.set(1);
        }

        let btnUp2 = () => {
            backToMenuButton.scale.set(1);
        }

        let btnAction2 = () => {
            core.StateManager.instance().setState(States.LAUNCHER, []);
        }

        let retryButton = new PIXI.Text('Новая игра', this.textStyle);
        retryButton.anchor.set(0.5);
        retryButton.x = gameConf.screen_width / 2 + 200;
        retryButton.y = gameConf.screen_height / 2 + 200;
        retryButton.visible = false;
        retryButton.interactive = true;
        retryButton.buttonMode = true;
        retryButton
            .on('mousedown', btnDown1)
            .on('mouseupoutside', btnUp1)
            .on('mouseup', btnAction2);
        retryButton.attachTo(winContainer);

        let backToMenuButton = new PIXI.Text('Выйти в меню', this.textStyle);
        backToMenuButton.anchor.set(0.5);
        backToMenuButton.x = gameConf.screen_width / 2 - 200;
        backToMenuButton.y = gameConf.screen_height / 2 + 200;
        backToMenuButton.visible = false;
        backToMenuButton.interactive = true;
        backToMenuButton.buttonMode = true;
        backToMenuButton
            .on('mousedown', btnDown2)
            .on('mouseupoutside', btnUp2)
            .on('mouseup', btnAction2);
        backToMenuButton.attachTo(winContainer);

        setTimeout(() => {
            retryButton.visible = true;
            backToMenuButton.visible = true;
        }, 1300);
    }

    eatCounter(figureNum) {
        let color = Figures.WHITE;
        let pics = this.whiteFiguresCounterPics;
        let obj = this.whiteFiguresCounterObj;
        let coef = 0;
        let correction = 0;

        if (figureNum > 6) {
            color = Figures.BLACK;
            pics = this.blackFiguresCounterPics;
            obj = this.blackFiguresCounterObj;
            coef = 6;
            correction = -50;
        }

        let countText = new PIXI.Text('x1');
        countText.x = 105;
        countText.anchor.y = 0.5;

        let index = obj.findIndex(item => item.figure == figureNum);

        if (index != -1) {
            obj[index].count++;
            pics[index][1].text = 'x' + obj[index].count;
        } else {
            obj.push({figure: figureNum, count: 1});

            let row = coef + obj.length;

            countText.y = row * 60 + 205 + correction;

            let figurePic = helper.createSprite(this.res, color, this, {x: 50, y: 0});
            figurePic.setColumn(figureNum - coef - 1);
            figurePic.width = 50;
            figurePic.height = 50;
            figurePic.y = row * 60 + 180 + correction;

            pics.push([figurePic, countText]);

            countText.attachTo(this);
        }
    }

    updateScore(whiteScore, blackScore) {
        this.whiteScore.text = whiteScore;
        this.blackScore.text = blackScore;
    }
}