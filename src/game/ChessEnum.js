const Figures = {
    KING: 0,
    QUEEN: 1,
    BISHOP: 2,
    KNIGHT: 3,
    ROOK: 4,
    PAWN: 5,
    WHITE: "whiteFigures",
    BLACK: "blackFigures"
}

export default Figures;