import GameView from "./GameView";
import GameModel from "./GameModel";

export default class GamePresenter {
    constructor() {
        this.availiableMoves = [];

        this.figureRow = -1;
        this.figureCol = -1;
    }

    show(parent) {
        this.view = new GameView();

        this.view.attachTo(parent);

        this.model = new GameModel();

        this.view.prepareBattleField(this.model.battleField);

        this.view.figures.forEach((row) => {
            row.forEach((item) => {
                item.on('isyourturn', e => {
                    let i = e.i;
                    let j = e.j;
                    if (this.model.isYourTurn(i, j)) {
                        let answer = this.view.selectPlaces(i, j, true);

                        this.availiableMoves = this.model.checkAvailiableMoves(i, j);

                        if (answer != -1) {
                            this.view.showAvailiableMoves(this.availiableMoves);
                            this.figureRow = i;
                            this.figureCol = j;
                        }
                    } else {
                        this.view.selectPlaces(i, j, false);
                    }
                });

                item.on('unselect', e => {
                    this.view.selectPlaces(e.i, e.j, false);
                });

                item.chessFigure.on('showghost', e => {
                    if (this.model.isYourTurn(e.i, e.j)) this.view.showGhost(e.event, e.col, e.color);
                });

                item.on('eat', e => {
                    this.eat(e.i, e.j);
                });
            });
        });

        this.view.squares.forEach((row) => {
            row.forEach((item) => {
                item.on('move', e => {
                    this.makeMove(e.i, e.j);
                })
            })
        })
    }

    hide() {
        this.view.detach();
    }

    makeMove(targetRow, targetCol) {
        this.model.move(this.figureRow, this.figureCol, targetRow, targetCol);
        this.view.move(this.figureRow, this.figureCol, targetRow, targetCol);
    }

    eat(targetRow, targetCol) {
        if (this.model.battleField[targetRow][targetCol] == 1) this.mate(1);
        else if (this.model.battleField[targetRow][targetCol] == 7) this.mate(7);
        else {
            this.view.eatCounter(this.model.battleField[targetRow][targetCol]);
            this.model.eat(this.figureRow, this.figureCol, targetRow, targetCol);
            this.view.eat(this.figureRow, this.figureCol, targetRow, targetCol);
        }

        this.view.updateScore(this.model.whiteScore, this.model.blackScore);
    }

    mate(who) {
        if (who > 1) this.view.mate("БЕЛЫЕ");
        else this.view.mate("ЧЁРНЫЕ");
    }
}