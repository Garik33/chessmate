import EE from 'eventemitter3'

export default class GameModel extends EE {
    constructor() {
        super();
        // 0  - empty
        // 1  - white king
        // 2  - white queen
        // 3  - white bishop
        // 4  - white knight
        // 5  - white rook
        // 6  - white pawn
        // 7  - black king
        // 8  - black queen
        // 9  - black bishop
        // 10 - black knight
        // 11 - black rook
        // 12 - black pawn
        this.battleField = [[11, 10,  9,  8,  7,  9, 10, 11],
                            [12, 12, 12, 12, 12, 12, 12, 12],
                            [ 0,  0,  0,  0,  0,  0,  0,  0],
                            [ 0,  0,  0,  0,  0,  0,  0,  0],
                            [ 0,  0,  0,  0,  0,  0,  0,  0],
                            [ 0,  0,  0,  0,  0,  0,  0,  0],
                            [ 6,  6,  6,  6,  6,  6,  6,  6],
                            [ 5,  4,  3,  2,  1,  3,  4,  5]];

        this.whiteFigures = [1, 2, 3, 4, 5, 6];
        this.blackFigures = [7, 8, 9, 10, 11, 12];

        this.whiteScore = 0;
        this.blackScore = 0;

        // this.eatedFigures = [];

        this.isWhiteTurn = true;
    }

    isYourTurn(i, j) {
        let selectedFigure = this.battleField[i][j];

        if ((this.blackFigures.includes(selectedFigure) && this.isWhiteTurn) || (this.whiteFigures.includes(selectedFigure) && !this.isWhiteTurn)) return false;

        return true;
    }

    checkAvailiableMoves(row, col) {
        let availiableMoves = [];
        let enemy;

        if (this.isWhiteTurn) enemy = this.blackFigures;
        else enemy = this.whiteFigures;

        let kingMoves = () => {
            let shiftedRow = row - 1;
            let shiftedCol = col - 1;
            for (let i = 0; i < 3; i++)
                for (let j = 0; j < 3; j++) {
                    if (i == 1 && j == 1) continue;
                    if (
                        this.battleField[shiftedRow + i] &&
                        (this.battleField[shiftedRow + i][shiftedCol + j] == 0 ||
                            enemy.includes(this.battleField[shiftedRow + i][shiftedCol + j]))
                    ) {
                        availiableMoves.push([shiftedRow + i, shiftedCol + j]);
                    }
                }
        }

        let rookMoves = () => {
            for (let i = row - 1; i >= 0; i--) {
                if (this.battleField[i][col] == 0) availiableMoves.push([i, col]);
                if (enemy.includes(this.battleField[i][col])) {
                    availiableMoves.push([i, col]);
                    break;
                }
                if (!enemy.includes(this.battleField[i][col]) && this.battleField[i][col] != 0) break;
            }

            for (let i = row + 1; i < 8; i++) {
                if (this.battleField[i][col] == 0) availiableMoves.push([i, col]);
                if (enemy.includes(this.battleField[i][col])) {
                    availiableMoves.push([i, col]);
                    break;
                }
                if (!enemy.includes(this.battleField[i][col]) && this.battleField[i][col] != 0) break;
            }

            for (let j = col + 1; j < 8; j++) {
                if (this.battleField[row][j] == 0) availiableMoves.push([row, j]);
                if (enemy.includes(this.battleField[row][j])) {
                    availiableMoves.push([row, j]);
                    break;
                }
                if (!enemy.includes(this.battleField[row][j]) && this.battleField[row][j] != 0) break;
            }

            for (let j = col - 1; j >= 0; j--) {
                if (this.battleField[row][j] == 0) availiableMoves.push([row, j]);
                if (enemy.includes(this.battleField[row][j])) {
                    availiableMoves.push([row, j]);
                    break;enemy.includes()
                }
                if (!enemy.includes(this.battleField[row][j]) && this.battleField[row][j] != 0) break;
            }
        }

        let knightMoves = () => {
            if (this.battleField[row - 2] && (this.battleField[row - 2][col - 1] == 0 || enemy.includes(this.battleField[row - 2][col - 1]))) availiableMoves.push([row - 2, col - 1]);
            if (this.battleField[row - 2] && (this.battleField[row - 2][col + 1] == 0 || enemy.includes(this.battleField[row - 2][col + 1]))) availiableMoves.push([row - 2, col + 1]);
            if (this.battleField[row - 1] && (this.battleField[row - 1][col + 2] == 0 || enemy.includes(this.battleField[row - 1][col + 2]))) availiableMoves.push([row - 1, col + 2]);
            if (this.battleField[row + 1] && (this.battleField[row + 1][col + 2] == 0 || enemy.includes(this.battleField[row + 1][col + 2]))) availiableMoves.push([row + 1, col + 2]);
            if (this.battleField[row + 2] && (this.battleField[row + 2][col + 1] == 0 || enemy.includes(this.battleField[row + 2][col + 1]))) availiableMoves.push([row + 2, col + 1]);
            if (this.battleField[row + 2] && (this.battleField[row + 2][col - 1] == 0 || enemy.includes(this.battleField[row + 2][col - 1]))) availiableMoves.push([row + 2, col - 1]);
            if (this.battleField[row + 1] && (this.battleField[row + 1][col - 2] == 0 || enemy.includes(this.battleField[row + 1][col - 2]))) availiableMoves.push([row + 1, col - 2]);
            if (this.battleField[row - 1] && (this.battleField[row - 1][col - 2] == 0 || enemy.includes(this.battleField[row - 1][col - 2]))) availiableMoves.push([row - 1, col - 2]);
        }

        let bishopMoves = () => {
            for (let i = 1; i < 8; i++) {
                if (this.battleField[row - i] && this.battleField[row - i][col - i] == 0) availiableMoves.push([row - i, col - i]);
                if (this.battleField[row - i] && enemy.includes(this.battleField[row - i][col - i])) {
                    availiableMoves.push([row - i, col - i]);
                    break;
                }
                if (this.battleField[row - i] && !enemy.includes(this.battleField[row - i][col - i]) && this.battleField[row - i][col - i] != 0) break;
            }

            for (let i = 1; i < 8; i++) {
                if (this.battleField[row - i] && this.battleField[row - i][col + i] == 0) availiableMoves.push([row - i, col + i]);
                if (this.battleField[row - i] && enemy.includes(this.battleField[row - i][col + i])) {
                    availiableMoves.push([row - i, col + i]);
                    break;
                }
                if (this.battleField[row - i] && !enemy.includes(this.battleField[row - i][col + i]) && this.battleField[row - i][col + i] != 0) break;
            }

            for (let i = 1; i < 8; i++) {
                if (this.battleField[row + i] && this.battleField[row + i][col + i] == 0) availiableMoves.push([row + i, col + i]);
                if (this.battleField[row + i] && enemy.includes(this.battleField[row + i][col + i])) {
                    availiableMoves.push([row + i, col + i]);
                    break;
                }
                if (this.battleField[row + i] && !enemy.includes(this.battleField[row + i][col + i]) && this.battleField[row + i][col + i] != 0) break;
            }

            for (let i = 1; i < 8; i++) {
                if (this.battleField[row + i] && this.battleField[row + i][col - i] == 0) availiableMoves.push([row + i, col - i]);
                if (this.battleField[row + i] && enemy.includes(this.battleField[row + i][col - i])) {
                    availiableMoves.push([row + i, col - i]);
                    break;
                }
                if (this.battleField[row + i] && !enemy.includes(this.battleField[row + i][col - i]) && this.battleField[row + i][col - i] != 0) break;
            }
        }

        let pawnMoves = () => {
            let targetRow;
            let additionalTargetRow;
            let baseLine;

            if (this.isWhiteTurn) {
                targetRow = row - 1;
                additionalTargetRow = targetRow - 1;
                baseLine = 6;
            }
            else {
                targetRow = row + 1;
                additionalTargetRow = targetRow + 1;
                baseLine = 1;
            }

            if (this.battleField[targetRow] && this.battleField[targetRow][col] == 0) availiableMoves.push([targetRow, col]);
            if (this.battleField[targetRow] && enemy.includes(this.battleField[targetRow][col - 1])) availiableMoves.push([targetRow, col - 1]);
            if (this.battleField[targetRow] && enemy.includes(this.battleField[targetRow][col + 1])) availiableMoves.push([targetRow, col + 1]);
            if (
                this.battleField[targetRow] &&
                this.battleField[additionalTargetRow] &&
                row == baseLine &&
                this.battleField[targetRow][col] == 0 &&
                this.battleField[additionalTargetRow][col] == 0
            ) {
                availiableMoves.push([additionalTargetRow, col]);
            }
        }

        switch (this.battleField[row][col]) {
            case 1:
            case 7: {
                kingMoves();

                break;
            }

            case 2:
            case 8: {
                bishopMoves();
                rookMoves();

                break;
            }

            case 3:
            case 9: {
                bishopMoves();

                break;
            }

            case 4:
            case 10: {
                knightMoves();

                break;
            }

            case 5:
            case 11: {
                rookMoves();

                break;
            }

            case 6:
            case 12: {
                pawnMoves();

                break;
            }
        }

        return availiableMoves;
    }

    move(figureRow, figureCol, targetRow, targetCol) {
        // console.log('model: from (' + figureRow + ', ' + figureCol + ') to (' + targetRow + ', ' + targetCol + ')');
        let temp = this.battleField[figureRow][figureCol];
        this.battleField[figureRow][figureCol] = this.battleField[targetRow][targetCol];
        this.battleField[targetRow][targetCol] = temp;
        this.isWhiteTurn = !this.isWhiteTurn;
    }

    eat(figureRow, figureCol, targetRow, targetCol) {
        // this.eatedFigures.push(this.battleField[targetRow][targetCol]);
        switch (this.battleField[targetRow][targetCol]) {
            case 2: {
                this.blackScore += 900;

                break;
            }

            case 3:
            case 4: {
                this.blackScore += 300;

                break;
            }

            case 5: {
                this.blackScore  += 500;

                break;
            }

            case 6: {
                this.blackScore += 100;

                break;
            }

            case 8: {
                this.whiteScore += 900;

                break;
            }

            case 9:
            case 10: {
                this.whiteScore += 300;

                break;
            }

            case 11: {
                this.whiteScore += 500;

                break;
            }

            case 12: {
                this.whiteScore += 100;

                break;
            }
        }
        this.battleField[targetRow][targetCol] = 0;
        this.move(figureRow, figureCol, targetRow, targetCol);
    }
}