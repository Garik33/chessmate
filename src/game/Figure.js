import Figures from "./ChessEnum";

export default class Figure extends PIXI.Container {
    constructor(startPositions, container, figureColor, i, j) {
        super('Figure');

        this.res = core.ResourceManager.instance().findResource('Chess');

        this.container = new PIXI.Container();
        this.container.x = j * 100;
        this.container.y = i * 100;
        this.container.attachTo(container);

        this.figureSubstate = new PIXI.Graphics();
        this.figureSubstate.beginFill(0x556B2F, 0.7);
        this.figureSubstate.drawRect(0, 0, 100, 100);
        this.figureSubstate.endFill();
        this.figureSubstate.attachTo(this.container);
        this.figureSubstate.visible = false;

        this.food = new core.ClipRect();
        this.food.width = 100;
        this.food.height = 100;
        this.food.attachTo(this.container);
        this.food.visible = false;

        this.circle = new PIXI.Graphics();
        this.circle.attachTo(this.food);

        const coefficient = (startPositions[i][j] > 6) ? 6 : 0;
        const col = (startPositions[i][j] - 1) - coefficient;
        // const figureColor = (startPositions[i][j] > 6) ? Figures.BLACK : Figures.WHITE;

        this.chessFigure = helper.createSprite(this.res, figureColor, this.container, { x: 0, y: 0 });
        this.chessFigure.setColumn(col);
        this.chessFigure.width = 100;
        this.chessFigure.height = 100;
        this.chessFigure.interactive = true;
        this.chessFigure.buttonMode = true;
        this.chessFigure
            .on('mousedown', (event) => {
                if (this.food.visible) this.emit('eat', { i: this.container.y / 100, j: this.container.x / 100 });

                this.emit('isyourturn', { i: this.container.y / 100, j: this.container.x / 100 });
                this.chessFigure.emit('showghost', { i: this.container.y / 100, j: this.container.x / 100, event: event, col: col, color: figureColor });
            })
            .on('mouseup', () => {
                if (this.food.visible) this.emit('eat', { i: this.container.y / 100, j: this.container.x / 100 });

                if (!this.figureSubstate.visible) this.emit('unselect', { i: this.container.y / 100, j: this.container.x / 100 });
            })
            .on('mouseover', () => {
                if (this.food.visible)  this.circle.visible = false;
            })
            .on('mouseout', () => {
                if (this.food.visible) this.circle.visible = true;
            });
    }

    toggleSelectionFigure() {
        this.figureSubstate.visible = !this.figureSubstate.visible;
    }

    getSquareColor() {
        if ((this.container.y / 100) % 2 === (this.container.x / 100) % 2) return 0xFFE4B5;
        else return 0xA0522D;
    }

    showFood() {
        this.food.visible = true;

        this.circle.beginFill(this.getSquareColor());
        this.circle.drawCircle(50, 50, 55);
        this.circle.endFill();
    }
}