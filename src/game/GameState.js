import States from "../States";
import GamePresenter from "./GamePresenter";

const core = require('../../../../work/web-core');

export default class GameState extends core.State {
    constructor() {
        super('Game State');

    }

    start() {
        console.log('Enter Game State');
        const gamePresenter = new GamePresenter();
        gamePresenter.show(this);
    }

    stop() {
        console.log('Exit Game State');
    }
}