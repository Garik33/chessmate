export default class Field extends PIXI.Container {
    constructor(container, i, j, color) {
        super('Field');

        this.container = new PIXI.Container();
        this.container.x = j * 100;
        this.container.y = i * 100;
        this.container.attachTo(container);

        this.square = new PIXI.Graphics();
        this.square.beginFill(color);
        this.square.drawRect(0, 0, 100, 100);
        this.square.endFill();
        // this.square.anchor.set(0.5);
        this.square.attachTo(this.container);
        this.square.interactive = true;
        this.square.buttonMode = true;
        this.square
            .on('mousedown', () => {
                if (this.substateForMove.visible) this.emit('move', {i: this.container.y / 100, j: this.container.x / 100});
            })
            .on('mouseup', () => {
                if (this.substateForMove.visible) this.emit('move', {i: this.container.y / 100, j: this.container.x / 100});
            });

        this.circle = new PIXI.Graphics();
        this.circle.beginFill(0x556B2F, 0.6);
        this.circle.drawCircle(50, 50, 15);
        this.circle.endFill();
        this.circle.attachTo(this.container);
        this.circle.visible = false;

        this.substateForMove = new PIXI.Graphics();
        this.substateForMove.beginFill(0x556B2F, 0.6);
        this.substateForMove.drawRect(0, 0, 100, 100);
        this.substateForMove.endFill();
        this.substateForMove.attachTo(this.container);
        this.substateForMove.visible = false;
    }

    toggleCircles() {
        this.circle.visible = !this.circle.visible;
    }

    toggleSubstate() {
        this.toggleCircles();
        this.substateForMove.visible = !this.substateForMove.visible;
    }
}