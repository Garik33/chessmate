import States from "../States";

const gameConf = require('./../../config/gameConf');
const core = require('../../../../work/web-core');

export default class LauncherState extends core.State {
    constructor() {
        super('Launcher State');

        this.res = core.ResourceManager.instance().findResource('Chess');

        const textStyle = new PIXI.TextStyle({
            fontFamily: 'Arial',
            fontSize: 36,
            fontStyle: 'italic',
            fontWeight: 'bold',
            fill: ['#ffffff', '#00ff99'], // gradient
            stroke: '#4a1850',
            strokeThickness: 5,
            dropShadow: true,
            dropShadowColor: '#000000',
            dropShadowBlur: 4,
            dropShadowAngle: Math.PI / 6,
            dropShadowDistance: 6,
            wordWrap: false,
            wordWrapWidth: 440
        });

        // const background = new PIXI.Graphics();
        // background.beginFill(0xFF69B4);
        // background.drawRect(0, 0, gameConf.screen_width, gameConf.screen_height);
        // background.endFill();
        // background.attachTo(this);

        helper.createSprite(this.res, 'anime_wallpaper1', this, {x: 0, y: 0});


        const welcomeText = 'Добро пожаловать в мои тестовые шахматы!';
        const welcomeMsg = new PIXI.Text(welcomeText, textStyle);
        welcomeMsg.anchor.set(0.5);
        welcomeMsg.x = gameConf.screen_width / 2;
        welcomeMsg.y = 200;
        // welcomeMsg.width = gameConf.screen_width;
        this.addChild(welcomeMsg);

        const startGame = () => {
            this.startButton.scale.set(1.0);
            core.StateManager.instance().setState(States.GAME, []);
        }

        const buttonDown = () => {
            this.startButton.scale.set(0.9);
        }

        const buttonUp = () => {
            this.startButton.scale.set(1.0);
        }

        const startButtonText = 'НАЧАТЬ';
        this.startButton = new PIXI.Text(startButtonText, textStyle);
        this.startButton.anchor.set(0.5);
        this.startButton.x = gameConf.screen_width / 2;
        this.startButton.y = gameConf.screen_height / 2;
        this.startButton.interactive = true;
        this.startButton.buttonMode = true;
        this.startButton
            .on('mousedown', buttonDown)
            .on('mouseupoutside', buttonUp)
            .on('mouseup', startGame);
        this.startButton.attachTo(this);
    }

    start() {
        console.log('Enter Launcher State');
    }

    stop() {
        console.log('Exit Launcher State');
    }
}